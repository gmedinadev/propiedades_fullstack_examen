import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import { Col, Row, Container } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";
import { connect } from "react-redux";

import store from "../redux/store";
import { getProperty } from "../redux/actionCreators";

const View = ({ properties, ...props }) => {

  let idFind = 2;    

  useEffect(() => {
    store.dispatch(getProperty(idFind));
  }, []);

  const handleDragStart = (e) => e.preventDefault();

  const objResp = {
    0: { items: 1 },
    568: { items: 3 },
    1024: { items: 5 },
  }

  const location = useLocation();

  /* id['id']) */

  let property_type = "";
  let property_operation = "";

  switch(properties.property_type) {
    case 'TERRAIN': property_type = 'Terreno'; break;
    default:        property_type = 'Undefined';
  }

  switch(properties.operation) {
    case 'RENT': property_operation = 'Arriendo'; break;
    default:     property_operation = 'Undefined';
  }

  
  const items = [
    <img src="https://images.pexels.com/photos/248547/pexels-photo-248547.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,
    <img src="https://images.pexels.com/photos/841130/pexels-photo-841130.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,
    <img src="https://images.pexels.com/photos/863977/pexels-photo-863977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,
    <img src="https://images.pexels.com/photos/841130/pexels-photo-841130.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,
    <img src="https://images.pexels.com/photos/863977/pexels-photo-863977.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,
    <img src="https://images.pexels.com/photos/248547/pexels-photo-248547.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="itemImg" onDragStart={handleDragStart} role="presentation" />,            
  ];
  
 /*
  const itemsLength = Array.from({ length: 5 });

  const items = itemsLength.map((item, index) => {
      const style = { height: 200 + index * 10 };
      return <div className="item" style={style} data-value={index + 1} />;
  });  
  */
  return (
    <div>
      <NavigationBar property={idFind} />
      <Container>
        <div>
          <Container>

            <Row>
                <Col>
                    <div className="cover">
                        <div>
                            <p className="col-md-8 m-0 p-0 fs-5">{property_type} - {property_operation}</p>
                            <p className="col-md-8 m-0 p-0 fs-3 fw-bold">{properties.street}</p>
                            <p className="col-md-8 m-0 p-0 fs-5">{properties.neighborhood} {properties.city} {properties.state}</p>
                            <p className="col-md-8 m-0 p-0 fs-3 fw-bold">{properties.price}</p>
                        </div>
                    </div>
                </Col>
            </Row>

            <Row>
                <Col className="mb-3">
                    <AliceCarousel 
                        mouseTracking
                        disableButtonsControls={true} 
                        disableDotsControls={true} 
                        items={items} 
                        responsive={objResp}
                        controlsStrategy="alternate"
                    />
                </Col>
            </Row>            

            <Row>
              <Col>
                <div className="card">
                    <div className="card-body">
                        <h6 className="card-subtitle mb-2">Detalles del inmueble</h6>
                        <p>{properties.description}</p>
                        <h6 className="card-subtitle mb-2">Características del inmueble</h6>
                        <Row>
                            <Col className="col-12 col-md-3 col-lg-2">
                                <ul>
                                    <li>abc</li>
                                    <li>abc</li>
                                    <li>abc</li>
                                </ul>
                            </Col>
                        </Row>
                    </div>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </Container>
    </div>
  );
};

const mapStateToProps = (state,id) => ({
  properties: state.propertyReducer.properties,
  id: id
});

export default connect(mapStateToProps, {})(View);
