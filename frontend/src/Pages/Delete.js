import React, { useEffect } from "react";
import { connect } from "react-redux";

import store from "../redux/store";
import { getProperty } from "../redux/actionCreators";

const Edit = ({ properties, ...props }) => {

  let idFind = 2;

  useEffect(() => {
    store.dispatch(getProperty(idFind));
  }, []);

  /* id['id']) */

};

const mapStateToProps = (state,id) => ({
  properties: state.propertyReducer.properties,
  id: id
});

export default connect(mapStateToProps, {})(Edit);
