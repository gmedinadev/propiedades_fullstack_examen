import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Col, Row, Container } from "react-bootstrap";
import NavigationBar from "../Components/Navbar";
import Mapbox from "../Components/Mapbox";
import { connect } from "react-redux";

import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";

const HomePage = ({ properties }) => {
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);

  return (
    <div>
      <NavigationBar property={null} />
      <Container>
        <div>
          <Container>
            <Row>
              <Col className="mt-2 mb-3">
                <Mapbox properties={properties} />
              </Col>
            </Row>

            {properties ? (
              <Row md={3} sm={2} xs={1}>
                {properties.map((property) => (
                  <Col key={property.id} className="mb-3">
                    <div className="card shadow-sm">
                      <Link to={'view/' + property.id}>
                        <img src="https://images.pexels.com/photos/207241/pexels-photo-207241.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260" className="card-img-top" alt={property.name} title={property.name} />
                      </Link>
                      <div className="card-body">
                        <div className="row">
                          <div className="col-4 text-center">
                            <Link to={'view/' + property.id}>
                              <i className="bi bi-eye-fill fs-4 text-dark m-0 p-0" title="Visualizar"></i>
                            </Link>                            
                          </div>
                          <div className="col-4 text-center">
                            <Link to={'edit/' + property.id}>
                              <i className="bi bi-pencil-fill fs-4 text-dark m-0 p-0" title="Editar"></i>
                            </Link>
                          </div>
                          <div className="col-4 text-center">
                            <Link to={'delete/' + property.id}>
                              <i className="bi bi-trash-fill fs-4 text-dark m-0 p-0" title="Eliminar"></i>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </Col>
                ))}
              </Row>
            ) : (
             <div></div>
            )}
          </Container>
        </div>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  properties: state.propertyReducer.properties,
});

export default connect(mapStateToProps, {})(HomePage);