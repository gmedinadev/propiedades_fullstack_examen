import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';

class Mapbox extends Component {

  static defaultProps = {
    center: {
      lat: 19.444260085812072,
      lng: -99.13869165935088
    },
    zoom: 11
  };

  

  render() {

    //console.log(this.props.properties.length);

    return (
      <div style={{ height: '50vh', width: '100%', borderRadius: 10 }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDR2HB3K5ETqT6kAzxUnJKxtsNl21bakX4' }}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >

{

  //this.props.properties.map( (item,i) => <CustomMarker key={i} lat={19.444260085812072} lng={-99.13869165935088} text="My Marker" /> )

}

          <CustomMarker lat={19.444260085812072} lng={-99.13869165935088} text="My Marker" />
          <CustomMarker lat={19.437318145862} lng={-99.15465273210364} text="Museo" />

        </GoogleMapReact>
      </div>
    );
  }
}

export default Mapbox;

const CustomMarker = ({ text }) => <div>{text}</div>