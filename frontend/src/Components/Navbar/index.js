import React, { Fragment, PureComponent } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";

const { Brand } = Navbar;
const NavLink = Nav.Link;

class NavigationBar extends PureComponent {

  closeSession = () => {
    localStorage.clear()
    window.location = "/"
  }

  render() {
    return (
      <Navbar collapseOnSelect sticky="top" expand="sm" bg="dark" variant="dark">
        <Container>
          <Brand>
            <NavLink href="/">Habi</NavLink>
          </Brand>
          { this.props.property ? <ControlBtn propertyId={this.props.property} /> : '' }
        </Container>
      </Navbar>
    );
  }
}

export default NavigationBar;

const ControlBtn = (propertyId) => {
  console.log(propertyId);
  return (
    <Fragment>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link href={'/edit/' + propertyId['propertyId']}><i className="bi bi-pencil-fill fs-4 me-2"></i><span className="d-md-none">Editar</span></Nav.Link>
          <Nav.Link href={'/delete/' + propertyId['propertyId']}><i className="bi bi-trash-fill fs-4 me-2"></i><span className="d-md-none">Eliminar</span></Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Fragment>
  );
}