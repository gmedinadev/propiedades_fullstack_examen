import { BrowserRouter, Route, Routes } from "react-router-dom";
import Delete from "./Pages/Delete";
import Edit from "./Pages/Edit";
import HomePage from "./Pages/Home";
import View from "./Pages/View";
import Page404 from "./Pages/Page404";

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />

        <Route path="/delete/:id" element={<Delete />} />
        <Route path="/edit/:id" element={<Edit />} />
        <Route path="/view/:id" element={<View />} />

        <Route path="*" element={<Page404 />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
